/* globals __dirname */
const webpack = require('webpack')
const path = require('path')

module.exports = {
	entry: {
		todos: [
			'webpack/hot/only-dev-server',
			path.resolve(__dirname, 'src', 'main.js')
		]
	},
	output: {
		path: path.resolve(__dirname, 'public'),
		publicPath: '/',
		filename: '[name].js'
	},
	module: {
		rules: [
			{
				test: /\.jsx?$/,
				loader: 'babel-loader',
				include: [
					path.resolve(__dirname, 'src'),
					/frontend-tools/
				],
				options: {
					cacheDirectory: true,
					plugins: [
						'transform-object-rest-spread',
						'transform-es2015-destructuring'
					],
					presets: ['react', 'es2015']
				}
			},
			{
				test: /\.less$/,
				use: [
					'style-loader',
					'css-loader',
					'postcss-loader',
					'less-loader'
				]
			}
		]
	},
	plugins: [
		new webpack.HotModuleReplacementPlugin(),
		new webpack.NoEmitOnErrorsPlugin(),
		new webpack.NamedModulesPlugin()
	],
	resolve: {
		alias: {
			'ft': path.resolve(__dirname, 'frontend-tools')
		},
		extensions: ['.js', '.jsx', '.json']
	},
	externals: {
		jquery: 'jQuery'
	},
	performance: {
		hints: false
	},
	watch: true,
	watchOptions: {
		poll: true
	},
	devServer: {
		hot: true,
		host: 'localhost',
		port: 8000,
		contentBase: [
			path.resolve(__dirname, 'public'),
			path.resolve(__dirname, 'node_modules/todomvc-app-css'),
			path.resolve(__dirname, 'node_modules/todomvc-common')
		]
	}
}
