/* globals __dirname */
const webpack = require('webpack')
const path = require('path')

module.exports = {
	entry: [
		'webpack/hot/only-dev-server',
		path.resolve(__dirname, 'demo.js')
	],
	output: {
		path: path.resolve(__dirname, 'public'),
		publicPath: '/',
		filename: 'demo.js'
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				loader: 'babel-loader',
				include: [path.resolve(__dirname)],
				options: {
					cacheDirectory: true,
					plugins: [
						'transform-object-rest-spread',
						'transform-es2015-destructuring'
					],
					presets: ['es2015']
				}
			},
			{
				test: /\.less$/,
				use: [
					'style-loader',
					'css-loader',
					'postcss-loader',
					'less-loader'
				]
			}
		]
	},
	plugins: [
		new webpack.HotModuleReplacementPlugin(),
		new webpack.NoErrorsPlugin(),
		new webpack.NamedModulesPlugin()
	],
	resolve: {
		extensions: ['.js']
	},
	externals: {
		jquery: 'jQuery'
	},
	performance: {
		hints: false
	},
	watch: true,
	watchOptions: {
		poll: true
	},
	devServer: {
		hot: true,
		host: 'localhost',
		port: 8000,
		contentBase: 'tester/public'
	}
}
