import jQuery from 'jquery'
import {DOM} from 'react'
import {render} from 'react-dom'
import jasmineRequire from 'jasmine-core/lib/jasmine-core/jasmine'
import './styles.less'

// Test runner
const jasmine = jasmineRequire.core(jasmineRequire)

export const runSpecs = specs => {
	const env = new jasmine.Env()
	let report = {total: 0, failed: 0, results: []}
	let depth = 0
	let context = report.results
	let deferred = jQuery.Deferred()
	env.addReporter({
		suiteStarted: ({description}) => {
			const suite = {type: 'suite', description, content: []}
			context.push(suite)
			context = suite.content
			depth++
		},
		specDone: ({description, failedExpectations, status}) => {
			context.push({
				type: 'spec',
				description,
				status,
				failedExpectations
			})
			report.total++
			report.failed += failedExpectations.length
		},
		suiteDone: () => {
			depth--
			context = report.results
			let i
			for (i = 0; i < depth; i++) {
				context = context[context.length - 1].content
			}
		},
		jasmineDone: () => {
			deferred.resolve(report)
		}
	})
	const jasmineInterface = jasmineRequire.interface(jasmine, env)
	const {createSpy, createSpyObj} = jasmineInterface.jasmine
	specs({...jasmineInterface, createSpy, createSpyObj})
	env.execute()
	return deferred.promise()
}


// Components
const {div, h1, h2} = DOM

export const mountTestReporter = (tests, id='tests') => {
	let el = document.getElementById(id)
	if (!el) {
		el = document.createElement('div')
		el.id = id
		document.body.appendChild(el)
	}
	let state = {
		total: 0,
		failed: 0,
		reports: tests.map(() => ({total: 0, failed: 0, results: []}))
	}
	tests.forEach((test, i) => {
		test.done(report => {
			state = {
				total: state.total + report.total,
				failed: state.failed + report.failed,
				reports: state.reports.map((r, j) => i === j ? report : r)
			}
			render(TestReporter(state), el)
		})
	})
}

const TestReporter = ({total, failed, reports}) => (
	div({className: 'test-reporter' + (failed ? ' has-failures' : '')},
		h1({className: 'title'}, 'Tests'),
		(total === 0
			? h2({className: 'no-tests'}, 'No tests yet.')
			: div({},
				h2({className: 'test-stats'}, `${total} tests, ${failed} failed`),
				div({className: 'test-reports'}, ...reports.map(Report))
			)
		)
	)
)

const Report = report => (
	div({className: 'test-report'}, ...report.results.map(ReportItem))
)

const ReportItem = reportResult => (
	reportResult.type === 'suite' ? SuiteReport(reportResult)
	: reportResult.type === 'spec' ? SpecReport(reportResult)
	: null
)

const SuiteReport = ({description, content}) => (
	div({className: 'test-suite'},
		div({className: 'suite-description'}, description),
		...content.map(ReportItem)
	)
)

const SpecReport = ({description, status, failedExpectations}) => (
	div({className: ['test-spec', status].join(' ')},
		description,
		(status === 'failed' ? SpecFailures(failedExpectations) : null)
	)
)

const SpecFailures = failedExpectations => (
	div({className: 'failed-specs'}, ...failedExpectations.map(SpecFailure))
)

const SpecFailure = ({message, stack}) => (
	div({className: 'failed-spec'},
		div({className: 'failed-spec-message'}, message),
		div({className: 'failed-spec-stack'}, stack)
	)
)
