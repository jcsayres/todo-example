import {mountTestReporter, runSpecs} from '.'

const specs1 = runSpecs(({describe, expect, it}) => {
	describe('simple test', () => {
		it('2 + 2 should be 4', () => {
			expect(2 + 2).toEqual(4)
		})
	})
})

const specs2 = runSpecs(({describe, expect, it}) => {
	describe('some nested tests', () => {
		it('the length of "hi" should be 2', () => {
			expect("hi".length).toEqual(2)
		})
		describe('one level deep', () => {
			it('"reverse" reversed should be "esrever"', () => {
				expect("reverse".split('').reverse().join('')).toEqual("esrever")
			})
		})
	})
})

const tests = [specs1, specs2]

mountTestReporter(tests)

if (module.hot) {
	module.hot.accept()
}
