import {runSpecs} from 'tester'
import {createStore} from 'ft'

const initialState = {
	name: 'Fred',
	friends: [
		{name: 'Wilma'},
		{name: 'Barney'},
		{name: 'Betty'}
	]
}

export default runSpecs(({createSpy, describe, expect, it}) => {
	describe('store', () => {
		describe('get', () => {
			it('should get the current state', () => {
				const store = createStore(initialState)
				expect(store.get()).toEqual(initialState)
			})
		})
		describe('set', () => {
			it('should set the current state', () => {
				const store = createStore(initialState)
				const nextState = {...initialState, name: 'Freddy'}
				store.set(nextState)
				expect(store.get()).not.toEqual(initialState)
				expect(store.get()).toEqual(nextState)
			})
		})
		describe('update', () => {
			it('should update the current state', () => {
				const store = createStore(initialState)
				const updater = state => ({...state, name: 'Freddy'})
				store.update(updater)
				expect(store.get()).not.toEqual(initialState)
				expect(store.get()).toEqual({...initialState, name: 'Freddy'})
			})
			it('should apply multiple updaters if provided', () => {
				const store = createStore(initialState)
				const updater1 = state => ({...state, name: 'Freddy'})
				const updater2 = state => ({...state, name: state.name + '2'})
				store.update(updater1, updater2)
				expect(store.get()).not.toEqual(initialState)
				expect(store.get()).toEqual({...initialState, name: 'Freddy2'})
			})
		})
		describe('view', () => {
			it('should return the result of calling the view function with the state', () => {
				const store = createStore(initialState)
				const view = state => state.name
				expect(store.view(view)).toEqual('Fred')
			})
		})
		describe('subscribe', () => {
			it('should call the provided callback immediately', () => {
				const store = createStore(initialState)
				const view = state => state.name
				const callback = createSpy()
				let id = store.subscribe(view, callback)
				store.unsubscribe(id)
				expect(callback).toHaveBeenCalledTimes(1)
			})
			it('should call the callback with the result of the view function', () => {
				const store = createStore(initialState)
				const view = state => state.name
				const callback = createSpy()
				let id = store.subscribe(view, callback)
				store.unsubscribe(id)
				expect(callback).toHaveBeenCalledWith('Fred')
			})
			it('should call the callback when the result of the view function changes', () => {
				const store = createStore(initialState)
				const view = state => state.name
				const callback = createSpy()
				let id = store.subscribe(view, callback)
				callback.calls.reset()
				store.update(state => ({...state, name: 'Wilma'}))
				store.unsubscribe(id)
				expect(callback).toHaveBeenCalledTimes(1)
				expect(callback).toHaveBeenCalledWith('Wilma')
			})
			it('should not call the callback if the result of the view function does not change', () => {
				const store = createStore(initialState)
				const view = state => state.name
				const callback = createSpy()
				let id = store.subscribe(view, callback)
				callback.calls.reset()
				store.update(state => ({...state, friends: []}))
				store.unsubscribe(id)
				expect(callback).not.toHaveBeenCalled()
			})
		})
		describe('unsubscribe', () => {
			it('should take the id returned by "subscribe" and cancel the subscription', () => {
				const store = createStore(initialState)
				const view = state => state.name
				const callback = createSpy()
				let id = store.subscribe(view, callback)
				callback.calls.reset()
				store.unsubscribe(id)
				store.update(state => ({...state, name: 'Wilma'}))
				expect(callback).not.toHaveBeenCalled()
			})
		})
		describe('transact', () => {
			it('should allow mutiple store updates before calling subscribers', () => {
				const store = createStore(initialState)
				const view = state => state.name
				const callback = createSpy()
				let id = store.subscribe(view, callback)
				callback.calls.reset()
				store.transact(() => {
					store.update(state => ({...state, name: 'Barney'}))
					store.update(state => ({...state, name: state.name + ' & Betty'}))
					store.update(state => ({...state, name: state.name + ' Rubble'}))
				})
				store.unsubscribe(id)
				expect(callback).toHaveBeenCalledTimes(1)
				expect(callback).toHaveBeenCalledWith('Barney & Betty Rubble')
			})
			it('should allow nested transactions, only notifying after the topmost one exits', () => {
				const store = createStore(initialState)
				const view = state => state.name
				const callback = createSpy()
				let id = store.subscribe(view, callback)
				callback.calls.reset()
				store.transact(() => {
					store.update(state => ({...state, name: 'Barney'}))
					store.update(state => ({...state, name: state.name + ' & Betty'}))
					store.update(state => ({...state, name: state.name + ' Rubble'}))
					store.transact(() => {
						store.update(state => ({...state, name: 'Bam Bam'}))
						store.update(state => ({...state, name: state.name + ' &'}))
						store.update(state => ({...state, name: state.name + ' Pebbles'}))
					})
					store.update(state => ({...state, name: state.name + '!!!'}))
				})
				store.unsubscribe(id)
				expect(callback).toHaveBeenCalledTimes(1)
				expect(callback).toHaveBeenCalledWith('Bam Bam & Pebbles!!!')
			})
		})
	})
})
