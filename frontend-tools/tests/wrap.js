import React from 'react'
import {runSpecs} from 'tester'
import {mount} from 'enzyme'
import {createStore, wrap} from 'ft'

export default runSpecs(({createSpy, describe, expect, it, spyOn}) => {
	describe('wrap', () => {
		describe('rendering efficiency', () => {
			it('should by default render the wrapped component only when shallow props have changed', () => {
				const TestComponent = wrap(() => null)
				const renderCheck = spyOn(TestComponent.prototype, 'render').and.callThrough()
				const el = mount(<TestComponent value={1} />)
				el.setProps({value: 1})
				el.setProps({value: 2})
				el.setProps({value: 2})
				el.setProps({value: 1})
				expect(renderCheck).toHaveBeenCalledTimes(3)
			})
			it('should have different rendering behavior if a different equality function is set', () => {
				const TestComponent = wrap(() => null)
				const renderCheck = spyOn(TestComponent.prototype, 'render').and.callThrough()
				const context = {equals: () => false}
				const el = mount(<TestComponent value={1} />, {context})
				el.setProps({value: 1})
				el.setProps({value: 2})
				el.setProps({value: 2})
				el.setProps({value: 1})
				expect(renderCheck).toHaveBeenCalledTimes(5)
			})
		})
		describe('aProps', () => {
			it('should provide the store to actions before they are passed to component', () => {
				const store = createStore({value: 2})
				const incBy = store => x => {
					store.update(appState => ({value: appState.value + x}))
				}
				const aProps = {incBy}
				const TestComponent = wrap(({incBy}) => <div onClick={() => incBy(3)} />, {aProps})
				const el = mount(<TestComponent />, {context: {store}})
				el.simulate('click')
				expect(store.get().value).toEqual(5)
			})
			it('should also provide the dispatch function so actions can call other actions', () => {
				const store = createStore({value: 2})
				const incBy = store => x => {
					store.update(appState => ({value: appState.value + x}))
				}
				const doubleThenIncBy = (store, dispatch) => x => {
					store.update(appState => ({value: 2 * appState.value}))
					dispatch(incBy, x)
				}
				const aProps = {doubleThenIncBy}
				const TestComponent = wrap(
					({doubleThenIncBy}) => <div onClick={() => doubleThenIncBy(3)} />,
					{aProps}
				)
				const el = mount(<TestComponent />, {context: {store}})
				el.simulate('click')
				expect(store.get().value).toEqual(7)
			})
		})
		describe('sProps', () => {
			it('should provide props with the results of the defined views', () => {
				const store = createStore({label: 'a', value: 2})
				const sProps = {combo: ({label, value}) => label + value}
				const renderCheck = createSpy()
				const TestComponent = wrap(props => (renderCheck(props), null), {sProps})
				mount(<TestComponent />, {context: {store}})
				expect(renderCheck).toHaveBeenCalledWith({combo: 'a2'})
			})
			it('should only re-render if any of the views has changed', () => {
				const store = createStore({label: 'a', value: 2})
				const sProps = {label: appState => appState.label, value: appState => appState.value}
				const renderCheck = createSpy()
				const TestComponent = wrap(props => (renderCheck(props), null), {sProps})
				mount(<TestComponent />, {context: {store}})
				store.set({label: 'a', value: 2})
				store.set({label: 'a', value: 3})
				store.set({label: 'b', value: 3})
				store.set({label: 'b', value: 3})
				store.set({label: 'b', value: 3})
				store.set({label: 'c', value: 3})
				expect(renderCheck).toHaveBeenCalledTimes(4)
				expect(renderCheck.calls.allArgs()).toEqual([
					[{label: 'a', value: 2}],
					[{label: 'a', value: 3}],
					[{label: 'b', value: 3}],
					[{label: 'c', value: 3}]
				])
			})
			it('should unsubscribe the views when the component unmounts', () => {
				const store = createStore({value: 2})
				const sProps = {value: appState => appState.value}
				const TestComponent = wrap(() => null, {sProps})
				const el = mount(<TestComponent />, {context: {store}})
				const unsubscribeCheck = spyOn(store, 'unsubscribe').and.callThrough()
				const setStateCheck = spyOn(TestComponent.prototype, 'setState').and.callThrough()
				el.unmount()
				store.set({value: 3})
				store.set({value: 4})
				store.set({value: 5})
				expect(unsubscribeCheck).toHaveBeenCalledTimes(1)
				expect(setStateCheck).toHaveBeenCalledTimes(0)
			})
		})
		describe('mProps', () => {
			const initialState = {value: 2, result: 'nothing yet'}
			const sProps = {value: appState => appState.value}
			it('can be used to map props to new ones', () => {
				const store = createStore(initialState)
				const mProps = props => ({
					newValue1: 2 * props.value,
					newValue2: props.something + ' bar'
				})
				const renderCheck = createSpy()
				const TestComponent = wrap(props => (renderCheck(props), null), {sProps, mProps})
				mount(<TestComponent something="foo" />, {context: {store}})
				expect(renderCheck).toHaveBeenCalledWith({newValue1: 4, newValue2: 'foo bar'})
			})
		})
		describe('mergeFn', () => {
			const initialState = {value: 2, result: 'nothing yet'}
			const action1 = store => () => store.update(state => ({...state, result: 'action1'}))
			const action2 = store => () => store.update(state => ({...state, result: 'action2'}))
			const aProps = {onClick: action1}
			const sProps = {value: appState => appState.value}
			it('should by default override aProps and sProps with passed in props', () => {
				const store = createStore(initialState)
				const renderCheck = createSpy()
				const TestComponent = wrap(props => (renderCheck(props), null), {aProps, sProps})
				mount(<TestComponent value={8} onClick={action2(store)} />, {context: {store}})
				const {value, onClick} = renderCheck.calls.mostRecent().args[0]
				onClick()
				expect(value).toEqual(8)
				expect(store.get().result).toEqual('action2')
			})
			it('should allow the default behavior to be overridden', () => {
				const store = createStore(initialState)
				const mergeFn = (props, extraProps) => ({...props, ...extraProps})
				const renderCheck = createSpy()
				const TestComponent = wrap(props => (renderCheck(props), null), {aProps, sProps, mergeFn})
				mount(<TestComponent value={8} onClick={action2(store)} />, {context: {store}})
				const {value, onClick} = renderCheck.calls.mostRecent().args[0]
				onClick()
				expect(value).toEqual(2)
				expect(store.get().result).toEqual('action1')
			})
		})
	})
})
