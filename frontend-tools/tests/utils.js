import {runSpecs} from 'tester'
import {createStore} from 'ft'
import {createDispatcher, find, mapObj, shallowEquals} from 'ft/src/utils'

export default runSpecs(({describe, expect, it}) => {
	describe('utils', () => {
		describe('createDispatcher', () => {
			it('should take a store and returns a dispatch function', () => {
				const store = createStore({})
				const dispatch = createDispatcher(store)
				const action1 = store => value => store.set({value: value + ' more'})
				const action2 = (store, dispatch) => value => {
					store.set({value})
					dispatch(action1, value + ' plus')
				}
				dispatch(action2, 'test')
				expect(store.get().value).toEqual('test plus more')
			})
		})
		describe('find', () => {
			it('should return an item in a list that satisfies a predicate', () => {
				const items = [{value: 'test'}, {value: 3}, {value: {x: 'y'}}]
				const pred = item => typeof item.value === 'object' && item.value.x === 'y'
				expect(find(pred, items)).toEqual({value: {x: 'y'}})
			})
			it('should return undefined if no item is found', () => {
				const items = [{value: 'test'}, {value: 3}, {value: {x: 'y'}}]
				const pred = item => item.value === 83
				expect(find(pred, items)).toBeUndefined()
			})
		})
		describe('mapObj', () => {
			it('should take a mapping function and an object and return a new object with mapped values', () => {
				const obj = {a: 1, b: 2, c: 3}
				const mapper = x => x * x
				expect(mapObj(mapper, obj)).toEqual({a: 1, b: 4, c: 9})
			})
			it('should do nothing to an empty object', () => {
				const obj = {}
				expect(mapObj(x => 2 * x, obj)).toBe(obj)
			})
		})
		describe('shallowEquals', () => {
			it('should return true for the same object', () => {
				const obj = {value: 1}
				expect(shallowEquals(obj, obj)).toBe(true)
			})
			it('should return true for two empty objects', () => {
				expect(shallowEquals({}, {})).toBe(true)
			})
			it('should return false if either argument is not an object', () => {
				expect(shallowEquals(1, {})).toBe(false)
				expect(shallowEquals('test', {})).toBe(false)
				expect(shallowEquals(null, {})).toBe(false)
				expect(shallowEquals(undefined, {})).toBe(false)
				expect(shallowEquals([], {})).toBe(false)
				expect(shallowEquals({}, 1)).toBe(false)
				expect(shallowEquals({}, 'test')).toBe(false)
				expect(shallowEquals({}, null)).toBe(false)
				expect(shallowEquals({}, undefined)).toBe(false)
				expect(shallowEquals({}, [])).toBe(false)
			})
			it('should compare each item in the objects by strict equality', () => {
				const value = {a: 'test'}
				expect(shallowEquals({value: 1}, {value: 1})).toBe(true)
				expect(shallowEquals({value: 'test'}, {value: 'test'})).toBe(true)
				expect(shallowEquals({value: null}, {value: null})).toBe(true)
				expect(shallowEquals({value: undefined}, {value: undefined})).toBe(true)
				expect(shallowEquals({value: [1]}, {value: [1]})).toBe(false)
				expect(shallowEquals({value: {a: 'test'}}, {value: {a: 'test'}})).toBe(false)
				expect(shallowEquals({value}, {value})).toBe(true)
			})
		})
	})
})
