import React from 'react'
import {runSpecs} from 'tester'
import {mount} from 'enzyme'
import {createApp} from 'ft'

export default runSpecs(({createSpy, describe, expect, it, spyOn}) => {
	describe('app', () => {
		it('should expose the base component', () => {
			const BaseComponent = () => null
			const app = createApp(BaseComponent, {})
			expect(app.baseComponent).toBe(BaseComponent)
		})
		it('should expose the store and set it to the initial state provided', () => {
			const app = createApp(() => {}, {value: 'test'})
			expect(app.store.get()).toEqual({value: 'test'})
		})
		it('should expose the Root element that can be mounted to start the app', () => {
			const app = createApp(() => <div className="check" />, {})
			const renderCheck = spyOn(app.Root.type.prototype, 'render').and.callThrough()
			const rootEl = mount(app.Root)
			expect(renderCheck).toHaveBeenCalled()
			expect(rootEl.html()).toEqual('<div class="check"></div>')
		})
		it('should expose the dispatch function that allows actions to be called', () => {
			const action = store => () => store.set({value: 42})
			const app = createApp(() => null, {})
			app.dispatch(action)
			expect(app.store.get()).toEqual({value: 42})
		})
		it('should set the store to a default blank state if no initial state is provided', () => {
			const app = createApp(() => {})
			expect(app.store.get()).toEqual({})
		})
		it('should render a base component that takes the full appState', () => {
			const renderCheck = createSpy()
			const BaseComponent = props => (renderCheck(props), null)
			const app = createApp(BaseComponent, {value: 'test'})
			mount(app.Root)
			expect(renderCheck).toHaveBeenCalledWith({value: 'test'})
		})
		it('should re-render the base component whenever the appState changes', () => {
			const renderCheck = createSpy()
			const BaseComponent = props => (renderCheck(props), null)
			const app = createApp(BaseComponent, {value: 'test1'})
			mount(app.Root)
			app.store.set({value: 'test1'})
			app.store.set({value: 'test1'})
			app.store.set({value: 'test2'})
			app.store.set({value: 'test2'})
			app.store.set({value: 'test2'})
			app.store.set({value: 'test3'})
			expect(renderCheck).toHaveBeenCalledTimes(3)
			expect(renderCheck.calls.allArgs()).toEqual([
				[{value: 'test1'}],
				[{value: 'test2'}],
				[{value: 'test3'}]
			])
		})
		it('should allow a custom equality function', () => {
			const renderCheck = createSpy()
			const BaseComponent = props => (renderCheck(props), null)
			const config = {equals: () => false}
			const app = createApp(BaseComponent, {value: 'test1'}, config)
			mount(app.Root)
			app.store.set({value: 'test1'})
			app.store.set({value: 'test1'})
			app.store.set({value: 'test2'})
			app.store.set({value: 'test2'})
			app.store.set({value: 'test2'})
			app.store.set({value: 'test3'})
			expect(renderCheck).toHaveBeenCalledTimes(7)
		})
		it('should set the context for children to hook into', () => {
			const equals = () => false
			const app = createApp(() => null, {}, {equals})
			const context = app.Root.type.prototype.getChildContext()
			expect(context.dispatch).toBe(app.dispatch)
			expect(context.equals).toBe(equals)
			expect(context.store).toBe(app.store)
		})
	})
	describe('middleware', () => {
		it('should be an array of fns that are each provided (action, args, next, dispatch)', () => {
			const actionSpy = createSpy()
			const action = () => actionSpy
			const middlewareSpy = createSpy()
			const middlewareFn = (...args) => {
				middlewareSpy(...args)
				args[2](args[0], ...args[1])
			}
			const middleware = [middlewareFn]
			const app = createApp(() => null, {}, {middleware})
			app.dispatch(action, 1, 'b')
			expect(middlewareSpy).toHaveBeenCalledTimes(1)
			const args = middlewareSpy.calls.argsFor(0)
			expect(args.length).toBe(4)
			expect(args[0]).toBe(action)
			expect(args[1]).toEqual([1, 'b'])
			expect(typeof args[2]).toBe('function')
			expect(typeof args[3]).toBe('function')
			expect(actionSpy).toHaveBeenCalledTimes(1)
			expect(actionSpy).toHaveBeenCalledWith(1, 'b')
		})
		it('should go to the next fn in the chain when "next" is called', () => {
			const actionSpy = createSpy()
			const action = () => actionSpy
			const middlewareSpy1 = createSpy()
			const middlewareSpy2 = createSpy()
			let n = 0
			const middlewareFn1 = (action, args, next) => {
				middlewareSpy1(n)
				n++
				next(action, ...args)
			}
			const middlewareFn2 = (action, args, next) => {
				middlewareSpy2(n)
				next(action, ...args)
			}
			const middleware = [middlewareFn1, middlewareFn2]
			const app = createApp(() => null, {}, {middleware})
			app.dispatch(action, 1, 'b')
			expect(middlewareSpy1).toHaveBeenCalledTimes(1)
			expect(middlewareSpy2).toHaveBeenCalledTimes(1)
			expect(middlewareSpy1).toHaveBeenCalledWith(0)
			expect(middlewareSpy2).toHaveBeenCalledWith(1)
			expect(actionSpy).toHaveBeenCalledTimes(1)
			expect(actionSpy).toHaveBeenCalledWith(1, 'b')
		})
		it('should start the middleware chain over if "dispatch" is called', () => {
			const actionSpy = createSpy()
			const action = () => actionSpy
			const middlewareSpy1 = createSpy()
			const middlewareSpy2 = createSpy()
			let n = 0
			const middlewareFn1 = (action, args, next, dispatch) => {
				middlewareSpy1(n)
				n++
				if (n > 3) {
					next(action, ...args)
				}
				else {
					dispatch(action, ...args)
				}
			}
			const middlewareFn2 = (action, args, next) => {
				middlewareSpy2(n)
				next(action, ...args)
			}
			const middleware = [middlewareFn1, middlewareFn2]
			const app = createApp(() => null, {}, {middleware})
			app.dispatch(action, 1, 'b')
			expect(middlewareSpy1).toHaveBeenCalledTimes(4)
			expect(middlewareSpy2).toHaveBeenCalledTimes(1)
			expect(middlewareSpy1.calls.allArgs()).toEqual([[0], [1], [2], [3]])
			expect(middlewareSpy2).toHaveBeenCalledWith(4)
			expect(actionSpy).toHaveBeenCalledTimes(1)
			expect(actionSpy).toHaveBeenCalledWith(1, 'b')
		})
	})
})
