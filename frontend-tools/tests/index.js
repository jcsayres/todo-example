import {mountTestReporter} from 'tester'
import appTests from './app'
import storeTests from './store'
import utilsTests from './utils'
import wrapTests from './wrap'

mountTestReporter([
	storeTests,
	wrapTests,
	appTests,
	utilsTests
])

if (module.hot) {
	module.hot.accept()
}
