/* globals __dirname */
const webpack = require('webpack')
const path = require('path')

module.exports = {
	entry: {
		demo: [
			'webpack/hot/only-dev-server',
			path.resolve(__dirname, 'demo', 'main.js')
		],
		tests: [
			'webpack/hot/only-dev-server',
			path.resolve(__dirname, 'tests', 'index.js')
		]
	},
	output: {
		path: path.resolve(__dirname, 'public'),
		publicPath: '/',
		filename: '[name].js'
	},
	module: {
		rules: [
			{
				test: /\.jsx?$/,
				loader: 'babel-loader',
				include: [
					path.resolve(__dirname),
					path.resolve(__dirname, '..', 'tester')
				],
				options: {
					cacheDirectory: true,
					plugins: [
						'transform-object-rest-spread',
						'transform-es2015-destructuring'
					],
					presets: ['react', 'es2015']
				}
			},
			{
				test: /\.less$/,
				use: [
					'style-loader',
					'css-loader',
					'postcss-loader',
					'less-loader'
				]
			}
		]
	},
	plugins: [
		new webpack.HotModuleReplacementPlugin(),
		new webpack.NoEmitOnErrorsPlugin(),
		new webpack.NamedModulesPlugin()
	],
	resolve: {
		alias: {
			'ft': path.resolve(__dirname),
			'tester': path.resolve(__dirname, '..', 'tester'),
			'~': path.resolve(__dirname, 'demo'),
		},
		extensions: ['.js', '.jsx', '.json']
	},
	externals: {
		jquery: 'jQuery',
		'react/addons': true,
		'react/lib/ExecutionEnvironment': true,
		'react/lib/ReactContext': true
	},
	performance: {
		hints: false
	},
	watch: true,
	watchOptions: {
		poll: true
	},
	devServer: {
		hot: true,
		host: 'localhost',
		port: 8000,
		contentBase: 'frontend-tools/public'
	}
}
