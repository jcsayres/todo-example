import ReactDOM from 'react-dom'
import {createApp} from 'ft'
import Main from '~/components/Main'

const logger = (action, args, next) => {
	// eslint-disable-next-line no-console
	console.log(`Action: ${action.name}, Args: ${JSON.stringify(args)}`)
	next(action, ...args)
}

const errorCatcher = (action, args, next) => {
	try {
		next(action, ...args)
	}
	catch (e) {
		// eslint-disable-next-line no-console
		console.error(e)
	}
}

if (global.app) {
	const app = global.app
	app.baseComponent = Main
	app.instance.forceUpdate()
}
else {
	const app = createApp(Main, {name: 'Fred'}, {middleware: [logger, errorCatcher]})
	global.app = app
	app.instance = ReactDOM.render(app.Root, global.document.getElementById('demo'))
}

if (module.hot) {
	module.hot.accept()
}
