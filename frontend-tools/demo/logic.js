export const capitalize = str => str.toUpperCase()

export const updateName = name => appState => ({...appState, name})
