import * as logic from './logic'

export const updateName = store => name => {
	store.update(logic.updateName(name))
}

export const testThrow = () => message => {
	throw message
}
