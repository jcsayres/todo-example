import React from 'react'
import Hello from './Hello'
import HelloEdit from './HelloEdit'
import HiThere from './HiThere'

export const Main = ({name}) => {
	return (
		<div className="main">
			<h1>Greeting</h1>
			<Hello id="my" className="name" name={name} />
			<h2>Edit</h2>
			<HelloEdit />
			<h2>Another sections</h2>
			<HiThere greeting="hi there" name={name} />
		</div>
	)
}

export default Main
