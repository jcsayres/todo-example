import React from 'react'
import {wrap} from 'ft'
import {capitalize} from '~/logic'

export const Hello = ({className, id, name}) => {
	return (
		<div className={className} id={id}>
			Howdy, {name}
		</div>
	)
}

const mProps = props => ({...props, name: capitalize(props.name)})
export default wrap(Hello, {mProps})
