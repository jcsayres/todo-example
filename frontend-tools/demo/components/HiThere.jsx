import React from 'react'
import {wrap} from 'ft'
import {testThrow} from '../actions'

export class HiThere extends React.Component {
	render() {
		const {greeting, name, testThrow} = this.props
		return (
			<div>
				<p>Greeting:</p>
				<div>{greeting}, {name}</div>
				<button onClick={() => testThrow('Oh no!!!')}>Click to throw</button>
			</div>
		)
	}
}

export default wrap(HiThere, {aProps: {testThrow}})
