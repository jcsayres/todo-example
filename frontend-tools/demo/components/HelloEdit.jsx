import React from 'react'
import {wrap} from 'ft'
import {updateName} from '~/actions'

export class HelloEdit extends React.Component {
	render() {
		const {name, updateName} = this.props
		return (
			<input value={name} onChange={e => updateName(e.target.value)} />
		)
	}
}

const aProps = {updateName}
const sProps = {name: appState => appState.name}
export default wrap(HelloEdit, {aProps, sProps})
