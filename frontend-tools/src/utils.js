export const createDispatcher = (store, middleware=[]) => {
	const dispatch = middleware
		.concat((action, ...args) => action(store, dispatch)(...args))
		.reverse()
		.reduce((next, mw) => mw ? (action, ...args) => mw(action, args, next, dispatch) : next)
	return dispatch
}

export const find = (predicate, items) => items.filter(predicate)[0]

export const mapObj = (fn, obj) => {
	const keys = Object.keys(obj)
	if (keys.length === 0) {
		return obj
	}
	const out = {}
	keys.forEach(key => {
		out[key] = fn(obj[key])
	})
	return out
}

export const shallowEquals = (obj1, obj2) => {
	if (!(obj1 instanceof Object) || !(obj2 instanceof Object)) {
		return false
	}
	if (obj1 instanceof Array || obj2 instanceof Array) {
		return false
	}
	if (obj1 === obj2) {
		return true
	}
	const obj1Keys = Object.keys(obj1)
	const obj2Keys = Object.keys(obj2)
	if (obj1Keys.length !== obj2Keys.length) {
		return false
	}
	let result = true
	for(var i = 0; i < obj1Keys.length; i++) {
		const key = obj1Keys[i]
		if (obj1[key] !== obj2[key]) {
			result = false
			break
		}
	}
	return result
}
