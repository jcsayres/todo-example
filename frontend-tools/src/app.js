import React from 'react'
import {createStore} from './store'
import {createDispatcher, shallowEquals} from './utils'

export const createApp = (baseComponent, initialState={}, config={}) => {
	const {equals=shallowEquals, middleware=[]} = config
	const store = createStore(initialState)
	const dispatch = createDispatcher(store, middleware)
	let app = {baseComponent, dispatch, store}
	class AppRoot extends React.Component {
		constructor(props) {
			super(props)
			this.state = {}
			this.subscriptionId = null
		}
		getChildContext() {
			return {dispatch, equals, store}
		}
		componentWillMount() {
			const view = appState => appState
			const callback = appState => this.setState(appState)
			this.subscriptionId = store.subscribe(view, callback)
		}
		shouldComponentUpdate(nextProps, nextState) {
			return !equals(nextState, this.state)
		}
		render() {
			return React.createElement(app.baseComponent, this.state)
		}
		componentWillUnmount() {
			store.unsubscribe(this.subscriptionId)
		}
	}
	AppRoot.displayName = 'AppRoot'
	AppRoot.childContextTypes = {
		dispatch: React.PropTypes.func,
		equals: React.PropTypes.func,
		store: React.PropTypes.object
	}
	app.Root = React.createElement(AppRoot, {})
	return app
}
