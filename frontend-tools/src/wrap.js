import React from 'react'
import {createDispatcher, mapObj, shallowEquals} from './utils'

const defaultMergeFn = (props, extraProps) => ({...extraProps, ...props})
const defaultMprops = props => props

export function wrap(WrappedComponent, options={}) {
	const {aProps={}, sProps={}, mProps=defaultMprops, mergeFn=defaultMergeFn} = options
	class Wrapper extends React.Component {
		constructor(props, context) {
			super(props)
			const dispatch = context.dispatch || createDispatcher(context.store)
			this.actionProps = {}
			Object.keys(aProps).forEach(key => {
				this.actionProps[key] = (...args) => {
					dispatch(aProps[key], ...args)
				}
			})
			this.subscriptionId = null
			this.subscriptionProps = {}
		}
		updateStateFromProps(props) {
			this.setState(
				mProps(mergeFn(props, {...this.actionProps, ...this.subscriptionProps}))
			)
		}
		componentWillMount() {
			if (Object.keys(sProps).length > 0) {
				const view = appState => mapObj(view => view(appState), sProps)
				const callback = subscriptionProps => {
					this.subscriptionProps = subscriptionProps
					this.updateStateFromProps(this.props)
				}
				this.subscriptionId = this.context.store.subscribe(view, callback)
			}
			else {
				this.updateStateFromProps(this.props)
			}
		}
		componentWillReceiveProps(nextProps) {
			if (!(this.context.equals || shallowEquals)(this.props, nextProps)) {
				this.updateStateFromProps(nextProps)
			}
		}
		shouldComponentUpdate(nextProps, nextState) {
			return !(this.context.equals || shallowEquals)(this.state, nextState)
		}
		render() {
			return React.createElement(WrappedComponent, this.state)
		}
		componentWillUnmount() {
			if (this.subscriptionId) {
				this.context.store.unsubscribe(this.subscriptionId)
				this.subscriptionId = null
			}
		}
	}
	Wrapper.displayName = (
		typeof WrappedComponent === 'function'
			? WrappedComponent.name
			: WrappedComponent.displayName
	) + 'Wrapper'
	Wrapper.contextTypes = {
		dispatch: React.PropTypes.func,
		equals: React.PropTypes.func,
		store: React.PropTypes.object
	}
	return Wrapper
}
