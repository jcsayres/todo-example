import {find} from './utils'

const defaultEquals = (x, y) => x === y

export const createStore = (initialState, equals=defaultEquals) => {
	let state = initialState
	let nextSubscriptionId = 1
	let subscriptions = []
	let inTransaction = false

	const get = () => state

	const view = fn => fn(state)

	const set = newState => {
		if (!equals(newState, state)) {
			state = newState
			if (!inTransaction) {
				checkSubscriptions()
			}
		}
	}

	const update = (...updaters) => set(updaters.reduce((s, u) => u(s), state))

	const checkSubscriptions = () => {
		const ids = subscriptions.map(s => s.id)
		ids.forEach(id => {
			const subscription = find(s => s.id === id, subscriptions)
			if (subscription) {
				subscription.check()
			}
		})
	}

	const subscribe = (viewer, callback) => {
		const id = nextSubscriptionId++
		let lastValue = viewer(state)
		function check() {
			const nextValue = viewer(state)
			if (!equals(lastValue, nextValue)) {
				lastValue = nextValue
				callback(lastValue)
			}
		}
		subscriptions.push({ id, check })
		callback(lastValue)
		return id
	}

	const unsubscribe = id => {
		subscriptions = subscriptions.filter(s => s.id !== id)
	}

	const transact = fn => {
		const alreadyInTransaction = inTransaction
		inTransaction = true
		fn()
		inTransaction = alreadyInTransaction
		if (!inTransaction) {
			checkSubscriptions()
		}
	}

	return {get, view, set, update, subscribe, unsubscribe, transact}
}
