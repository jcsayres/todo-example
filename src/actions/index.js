import * as updaters from '../logic/updaters'

export const toggleCompleted = store => id => {
	store.update(updaters.toggleCompleted(id))
}

export const toggleAllCompleted = store => () => {
	store.update(updaters.toggleAllCompleted)
}

export const removeTodo = store => id => {
	store.update(updaters.removeTodo(id))
}

export const setTodoEditModeOn = store => id => {
	store.update(updaters.setTodoEditModeOn(id))
}

export const setTodoEditModeOff = store => () => {
	store.update(updaters.setTodoEditModeOff)
}

export const setTodoDescription = store => (id, description) => {
	store.update(updaters.setTodoDescription(id, description))
}


// export const addTodo = ({logic, store}) => () => {
//     update(logic.addTodo, store)
// }
//
// export const updateNewTodo = ({store}) => description => {
// 	mergeAt('newTodo', {description}, store)
// }
//
//
// export const removeTodo = ({store}) => id => {
// 	updateAt('todos', reject(todo => todo.id === id), store)
// }
//
// export const toggleCompletedAll = ({logic, store}) => () => {
// 	updateAt('todos', logic.toggleCompletedAll, store)
// }
//
// export const setEditMode = ({logic, store}) => id => {
// 	updateAt('todos', logic.setEditMode(id), store)
// }
//
// export const clearEditMode = ({actions, logic, store}) => () => {
// 	updateAt('todos', pipe(logic.clearEditMode, logic.removeEmptyTodos), store)
// }
//
// export const updateTodoDescription = ({logic, store}) => (id, description) => {
// 	updateAt('todos', logic.updateTodoDescription(id, description), store)
// }
