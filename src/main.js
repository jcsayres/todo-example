import ReactDOM from 'react-dom'
import {createApp} from 'ft'
import TodoApp from './components/TodoApp'
import {initialState} from './logic/app'

if (global.app) {
	const app = global.app
	app.baseComponent = TodoApp
	app.instance.forceUpdate()
}
else {
	const app = createApp(TodoApp, initialState)
	global.app = app
	app.instance = ReactDOM.render(app.Root, global.document.getElementById('todos'))
}

if (module.hot) {
	module.hot.accept()
}
