import * as appState from '../logic/app'
import * as todoList from '../logic/todoList'

export const toggleCompleted = id =>
	appState.updateTodos(todoList.toggleTodoCompleted(id))

export const toggleAllCompleted =
	appState.updateTodos(todoList.toggleAllCompleted)

export const removeTodo = id =>
	appState.updateTodos(todoList.removeTodo(id))

export const setTodoEditModeOn = id =>
	appState.updateTodos(todoList.setTodoEditModeOn(id))

export const setTodoEditModeOff =
	appState.updateTodos(todoList.setTodoEditModeOff)

export const setTodoDescription = (id, description) =>
	appState.updateTodos(todoList.setTodoDescription(id, description))
