import {
	setCompleted, setDescription, setEditModeOn, setEditModeOff, toggleCompleted
} from './todo'

export const areAllCompleted = todos => todos.every(todo => todo.completed)

export const toggleAllCompleted = todos => {
	var value = !areAllCompleted(todos)
	return todos.map(setCompleted(value))
}

export const toggleTodoCompleted = id => todos => {
	return todos.map(todo => todo.id === id ? toggleCompleted(todo) : todo)
}

export const removeTodo = id => todos => {
	return todos.filter(todo => todo.id !== id)
}

export const getNumActiveTodos = todos => {
	return todos.filter(todo => !todo.completed).length
}

export const setTodoEditModeOn = id => todos => {
	return todos.map(todo => todo.id === id ? setEditModeOn(todo) : todo)
}

export const setTodoEditModeOff = todos => {
	return todos.map(setEditModeOff)
}

export const setTodoDescription = (id, description) => todos => {
	return todos.map(todo => todo.id === id ? setDescription(description)(todo) : todo)
}
