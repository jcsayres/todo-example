export const makeUpdater = (getter, setter) => updater => obj => {
	return setter(updater(getter(obj)))(obj)
}
