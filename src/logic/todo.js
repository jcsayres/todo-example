export const defaultTodo = {
	id: 0,
	description: ''
}

export const incrementId = todo => ({...todo, id: todo.id + 1})

export const setCompleted = value => todo => ({...todo, completed: value})

export const toggleCompleted = todo => setCompleted(!todo.completed)(todo)

export const setEditModeOn = todo => ({...todo, editing: true})

export const setEditModeOff = todo => ({...todo, editing: false})

export const setDescription = description => todo => ({...todo, description})
