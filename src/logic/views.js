import R from 'ramda'
import * as appState from './app'
import * as todoList from './todoList'

export const newTodo = appState.getNewTodo

export const todos = appState.getTodos

export const allCompleted = R.pipe(appState.getTodos, todoList.areAllCompleted)

export const numActiveTodos = R.pipe(appState.getTodos, todoList.getNumActiveTodos)
