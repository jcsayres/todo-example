import {makeUpdater} from './utils'

export const initialState = {
	newTodo: {
		id: 4,
		description: ''
	},
	todos: [
		{id: 0, description: 'do laundry', completed: false, editing: false},
		{id: 1, description: 'go shopping', completed: true, editing: false},
		{id: 2, description: 'drink beer', completed: false, editing: false},
		{id: 3, description: 'clean bike', completed: true, editing: false}
	]
}

export const getNewTodo = appState => appState.getNewTodo

export const setNewTodo = newTodo => appState => ({...appState, newTodo})

export const updateNewTodo = makeUpdater(getNewTodo, setNewTodo)

export const getTodos = appState => appState.todos

export const setTodos = todos => appState => ({...appState, todos})

export const updateTodos = makeUpdater(getTodos, setTodos)

// export const addTodo = appState => {
// 	const {newTodo, todos} = appState
// 	if (!newTodo.description) {
// 		return appState
// 	}
// 	const todo = {...newTodo, completed: false, editing: false}
// 	return {
// 		newTodo: {id: newTodo.id + 1, description: ''},
// 		todos: append(todo, todos)
// 	}
// }
//
// export const toggleCompleted = curry(
// 	(id, todos) => todos.map(todo => (
// 		todo.id === id
// 			? updateAt('completed', not, todo)
// 			: todo
// 	))
// )
//
// export const areAllCompleted = todos => all(getAt('completed'), todos)
//
// export const toggleCompletedAll = todos => (
// 	todos.map(setAt('completed', !areAllCompleted(todos)))
// )
//
// export const setEditMode = curry(
// 	(id, todos) => todos.map(todo => setAt('editing', todo.id === id, todo))
// )
//
// export const clearEditMode = todos => todos.map(setAt('editing', false))
//
// export const updateTodoDescription = curry(
// 	(id, description, todos) => todos.map(todo => (
// 		todo.id === id
// 			? setAt('description', description, todo)
// 			: todo
// 	))
// )
//
// export const removeEmptyTodos = todos => todos.filter(getAt('description'))
