import React from 'react'
import EditTodo from './EditTodo'
import TodoView from './TodoView'

export const Todo = ({todo}) => {
	const className = todo.editing ? 'editing' : ''
	return (
		<li className={className}>
			{todo.editing
				? <EditTodo todo={todo} />
				: <TodoView todo={todo} />
			}
		</li>
	)
}

export default Todo

// export const Todo = ({actions}) => ({todo}) => {
// 	const {id, description, completed, editing} = todo
// 	const className = makeClassName({completed, editing})
// 	return (
// 		li({className}, [
// 			(editing
// 				? input('.edit', {
// 					type: 'text',
// 					value: description,
// 					autoFocus: true,
// 					onChange: e => actions.updateTodoDescription(id, e.target.value),
// 					onBlur: actions.clearEditMode
// 				})
// 				: div('.view', [
// 					input('.toggle', {
// 						type: 'checkbox',
// 						checked: completed,
// 						onChange: () => actions.toggleCompleted(id)
// 					}),
// 					label({onDoubleClick: () => actions.setEditMode(id)}, [description]),
// 					button('.destroy', {
// 						onClick: () => actions.removeTodo(id)
// 					})
// 				])
// 			)
// 		])
// 	)
// }
