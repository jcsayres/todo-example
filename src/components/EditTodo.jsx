import React from 'react'
import {wrap} from 'ft'
import {setTodoDescription, setTodoEditModeOff} from '../actions'

export const EditTodo = ({setTodoEditModeOff, setTodoDescription, todo}) => {
	return (
		<input className="edit"
			type="text"
			value={todo.description}
			autoFocus={true}
			onChange={e => setTodoDescription(todo.id, e.target.value)}
			onBlur={setTodoEditModeOff} />
	)
}

const aProps = {setTodoEditModeOff, setTodoDescription}
export default wrap(EditTodo, {aProps})
