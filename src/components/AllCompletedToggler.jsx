import React from 'react'
import {wrap} from 'ft'
import {allCompleted} from '../logic/views'
import {toggleAllCompleted} from '../actions'

export const AllCompletedToggler = ({allCompleted, toggleAllCompleted}) => {
	return (
		<input className="toggle-all"
			type="checkbox"
			checked={allCompleted}
			onChange={toggleAllCompleted} />
	)
}

const aProps = {toggleAllCompleted}
const sProps = {allCompleted}
export default wrap(AllCompletedToggler, {aProps, sProps})
