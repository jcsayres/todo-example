import React from 'react'
import {wrap} from 'ft'
import AllCompletedToggler from './AllCompletedToggler'
import TodoList from './TodoList'
import {numActiveTodos} from '../logic/views'

export const TodoApp = ({numActiveTodos}) => {
	// const {newTodo, todos} = appState
	return (
		<div>
			<section className="todoapp">
				<header className="header">
					<h1>todos</h1>
				</header>
				<section className="main">
					<TodoList />
					<AllCompletedToggler />
				</section>
				<footer className="footer">
					<span className="todo-count">
						<strong></strong>
						{numActiveTodos} item{numActiveTodos === 1 ? '' : 's'} left
					</span>
				</footer>
			</section>
			<footer className="info">
				<p>Double-click to edit a todo</p>
			</footer>
		</div>
	)
}

const sProps = {numActiveTodos}
export default wrap(TodoApp, {sProps})
