import React from 'react'
import {wrap} from 'ft'
import {removeTodo, setTodoEditModeOn, toggleCompleted} from '../actions'

export const TodoView = ({todo, removeTodo, setTodoEditModeOn, toggleCompleted}) => {
	return (
		<div className="view">
			<input className="toggle"
				type="checkbox"
				checked={todo.completed}
				onChange={() => toggleCompleted(todo.id)}
				/>
			<label onDoubleClick={() => setTodoEditModeOn(todo.id)}>
				{todo.description}
			</label>
			<button className="destroy"
				onClick={() => removeTodo(todo.id)} />
		</div>
	)
}

const aProps = {removeTodo, setTodoEditModeOn, toggleCompleted}
export default wrap(TodoView, {aProps})

// 				: div('.view', [
// 					input('.toggle', {
// 						type: 'checkbox',
// 						checked: completed,
// 						onChange: () => actions.toggleCompleted(id)
// 					}),
// 					label({onDoubleClick: () => actions.setEditMode(id)}, [description]),
// 					button('.destroy', {
// 						onClick: () => actions.removeTodo(id)
// 					})
// 				])
// 			)
