import React from 'react'
import {wrap} from 'ft'
import Todo from './Todo'
import {todos} from '../logic/views'

export const TodoList = ({todos}) => {
	return (
		<ul className="todo-list">
			{todos.map(todo => <Todo key={todo.id} todo={todo} />)}
		</ul>
	)
}

const sProps = {todos}
export default wrap(TodoList, {sProps})
